﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcPhotoAlbum.Controllers;
using MvcPhotoAlbum.Models;


namespace MvcPhotoAlbum.Tests
{
   [TestClass]
   public class HomeControllerTest
   {
      [TestMethod]
      public void IndexTest()
      {
         //Arrange
         HomeController controller = new HomeController();
         //Act
         ViewResult result = controller.Index();

         //Assert
         Assert.AreEqual(result.ViewBag.Greeting, DateTime.Now.Hour < 12 ?
            "Good morning" : "Good afternoon");
       }

      [TestMethod]
      public void StoreIndex()
      {
         //Arrange
         AlbumController aController = new AlbumController();
         //Act
         ViewResult result = aController.Index();
         //Assert
         Assert.IsNotNull(result.ViewBag.categories);
      }

      [TestMethod]
      public void AlbumBrowse()
      {
         //Arrange
         AlbumController bController = new AlbumController();
       
         //Act
        // var typeModel = new Category("lucky");
      //  ViewResult result = bController.Browse("lucky" );        
        
         //Assert
      //  Assert.AreNotEqual(result.ViewBag.Browse, result );
         
      }

      [TestMethod]
      public void CreateAlbumTest()
      {
         //Arrange
         Album aAlbum = new Album("Vacaciones en Mexico");
         const string TITLE = "test title";
         DateTime dateTaken= DateTime.Today;
         User anUser= new User("me", "here@ho.com");
                  
         //Act 
         aAlbum.Title = TITLE;
         aAlbum.DateTaken = dateTaken;
        // aAlbum.UserName = anUser;
         //Assert
         Assert.AreEqual(TITLE, aAlbum.Title);
         Assert.AreEqual(dateTaken, aAlbum.DateTaken);
        // Assert.AreEqual(anUser, aAlbum.UserName);
      }

      [TestMethod]
      public void addPhotoTest()
      {
         ////Arrange
         //Album aAlbum = new Album("vacaciones");
         //const string TEXT1 = "filename test1";
         //const string TEXT2 = "filename test2";
         //string today = DateTime.Today.ToString();
         ////Act
         //User user1 = new User("User1", "ye@hotmail.com");
         //User user2 = new User("user2", "yi@hotmail.com");
         //aAlbum.addPhotos(TEXT1, user1);
         //aAlbum.addPhotos(TEXT2, user2);
         ////Assert
         //Assert.AreEqual(TEXT1, aAlbum.Photos[0].FileName);
         //Assert.AreEqual(user1, aAlbum.Photos[0].UserName);
         //Assert.AreEqual(TEXT2, aAlbum.Photos[1].FileName);
         //Assert.AreEqual(user2, aAlbum.Photos[1].UserName);
        // Assert.AreNotEqual(new User("yo"), aAlbum.PhotoList[0].FileName);
       }

      [TestMethod]
      public void addCategoryTest()
      {
         Album newAlbum = new Album("testalbum");
         string mystring = "teststring";
        // newAlbum.Categories.Add(new Category(mystring));
         newAlbum.Categories.Add(new Category(mystring));
         //Assert
        // Assert.AreEqual(mystring, newAlbum.Categories[0].CategoryName);
         //Assert.AreNotEqual("hey", newAlbum.Categories[0].CategoryName);

      }

      [TestMethod]
      public void addUserTest()
      {
         //Arrange
         Album oneMoreAlbum = new Album("onemoretest");
         //Act
         string myUserString = "Reina";
         string myemail = "hey@gotm.com";
         User theUserName= new User(myUserString, myemail);
      //    oneMoreAlbum.UserName=theUserName;
         //Assert
       // Assert.AreEqual(theUserName, oneMoreAlbum.UserName);
        // Assert.AreNotEqual("lucia",oneMoreAlbum.UserName);
      }

      //[TestMethod]
      //public void add



   }
}
