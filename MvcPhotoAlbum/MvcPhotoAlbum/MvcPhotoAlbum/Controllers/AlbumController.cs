﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPhotoAlbum.Models;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using MvcPhotoAlbum.Models.ViewModels;
using System.Web.Helpers;
using System.Text;
using WebMatrix.Data;
using WebMatrix.WebData;
using System.Web.Security;
using MvcPhotoAlbum.Filters;
using System.Net;





 
namespace MvcPhotoAlbum.Controllers
{
   
   [Authorize]
   [InitializeSimpleMembership]
   public class AlbumController : Controller
   {
      PhotoAlbumContext db = new PhotoAlbumContext();   
    
      [HttpGet]
      public ViewResult Index()
      {
        var i = WebSecurity.GetUserId(User.Identity.Name);
           
            var Categories = db.Categories.ToList();
         
          //  var Albums = db.Albums.ToList();
            var Albums=(from d in db.Albums where d.UserId == i select d).ToList();

            ViewBag.Categories = new SelectList(db.Categories, "CategoryId", "CategoryName");          

            return View(Albums);         
      }


      public ActionResult Search(FormCollection collection, string searchString, int? theCategoryId)
      {        
         var i = WebSecurity.GetUserId(User.Identity.Name);

         var pics = from m in db.Albums
                    where m.UserId == i
                    select m;        //all albums for logged in user

         List<Album> albumList = new List<Album>();//empty list will be returned if nothing is found 

         if (searchString == "" && theCategoryId == null)
         {
            ModelState.AddModelError("Title", "Looks like you want to find your album by name, please enter a name ");
            return View(albumList);
         }         
            else  if (theCategoryId != null)
               {
                  int CatId = Convert.ToInt32(theCategoryId);
                  ViewBag.Categories = new SelectList(db.Categories, "CategoryId", "CategoryName");
                  List<Album> myAlbumList = (from d in db.Albums where d.UserId == i select d).ToList(); //List of albums for current user
           
                  foreach (Album a in myAlbumList)//iterate through all albums for current user
                  {
                     //HashSet of all the Categories in an album
                     var albumCat = new HashSet<int>(a.Categories.Select(c => c.CategoryId));

                       if (albumCat.Contains(CatId))
                       {
                        albumList.Add(a);
                      
                       }
                   
                     }
                  return View(albumList);
                  }
               

      
              else if (!String.IsNullOrEmpty(searchString))
               {

                  pics = pics.Where(s => s.Title.Contains(searchString));//looking in the title
                  return View(pics);
                 
               }

         return View(pics);   
    }

      public ActionResult Details(int id = 0)
      {
         Album myAlbum = db.Albums.Find(id);

         if (myAlbum == null)
         {
            return HttpNotFound();
         }
         else
         {
            var photoList = (from p in db.Photos
                             where p.AlbumId == id
                             select p).ToList();

            List<Photo> foundList = new List<Photo>();

            foreach (Photo p in photoList)
            {
               if (p.Picture != null)
               {
                  foundList.Add(p);
               }
            }
            return View(foundList.ToList());
         }
      }

      /// <summary>
      /// ////////*work under this line */////
      /// </summary>
      /// <param name="id"></param>
      ///// <returns></returns>
      public ActionResult GetImage(int id)
      {
         var thePicture = db.Photos.Find(id);

         if (thePicture.Picture != null)

            return File(thePicture.Picture, "image/type");

         return null;
      }
           

      public ActionResult Create()
      {
         
         return View();//added imagemodel
      }


      //
      // POST: /Album/Create
      [HttpPost]
      public ActionResult Create(Album album)
      {
          var i = WebSecurity.GetUserId(User.Identity.Name);

         var result = (from a in db.Albums
                      where a.Title == album.Title
                      && (a.UserId == i)
                      select a);        
         try
         {            
               if (result.Count() > 0) //if I find title in table
               {
                  ModelState.AddModelError("Title", "You already have an album with that name");
                  return View();
               }

             if (album.DateTaken > DateTime.Today) //if entered date is later than todays 
               {
                  ModelState.AddModelError("DateTaken", "That's a future date!!");
                  return View();
               }                 
             
           else   if (ModelState.IsValid)
            {
                 album.UserId = i;  //user id
                  db.Albums.Add(album);
                  db.SaveChanges();    
                   return RedirectToAction("Index");
             }              

         }
         catch (DataException)
        {
           // Log the error (add a variable name after DataException)
            ModelState.AddModelError("button", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
         }
         return View();
      }


      private void PopulateAlbumCat(Album album)
      {
         var allCat = db.Categories;
         var albumCat = new HashSet<int>(album.Categories.Select(c => c.CategoryId));
         var viewModel = new List<SelectedCat>();

         foreach (var cat in allCat)//iterate thgough all categories
         {
            viewModel.Add(new SelectedCat
            {
               CategoryId = cat.CategoryId,
               CategoryName = cat.CategoryName,
               Selected = albumCat.Contains(cat.CategoryId)
            });
         }
         ViewBag.Categories = viewModel;
      }


      private void UpdateAlbumCategories(string[] checkedCategories, Album albumToUpdate)
      {
         if (checkedCategories == null)
         {
            albumToUpdate.Categories = new List<Category>();
            return;
         }
         var checkedCatHS = new HashSet<string>(checkedCategories);
         var albumCategories = new HashSet<int>(albumToUpdate.Categories.Select(c => c.CategoryId));
         foreach (var category in db.Categories)
         {
            if (checkedCatHS.Contains(category.CategoryId.ToString()))
            {
               if (!albumCategories.Contains(category.CategoryId))
               {
                  albumToUpdate.Categories.Add(category);
               }
            }
            else
            {
               if (albumCategories.Contains(category.CategoryId))
               {
                  albumToUpdate.Categories.Remove(category);
               }
            }
         }


      }


      private void AddOrUpdateCat(Album album, IEnumerable<AssignedCat> assignedCategories)
      {
         if (assignedCategories != null)
         {
            foreach (var assignedCat in assignedCategories)
            {
               if (assignedCat.Assigned)
               {
                  var category = new Category { CategoryId = assignedCat.CategoryId };
                  db.Categories.Attach(category);
                  album.Categories.Add(category);
               }

            }

         }
      }

      //GET
      public ActionResult Edit(int id)
      {
         Album album = db.Albums
         .Include(i => i.Categories)
         .Where(i => i.AlbumId == id)
         .Single();
         PopulateAlbumCat(album);

         TempData["id"] = id;
         return View(album);

      }



      [HttpPost]
      public ActionResult Edit(int id, FormCollection formCollection, string[] SelectedCat)
      {//Album album,

         var albumToUpdate = db.Albums
            .Include(t => t.Categories)
             .Single(t => t.AlbumId == id);

         if (TryUpdateModel(albumToUpdate, "", null, new string[] { "Categories" }))
         //if (TryUpdateModel(albumToUpdate, "", new string[] { "Albums" }))
         {
            try
            {
               UpdateAlbumCategories(SelectedCat, albumToUpdate);

               db.Entry(albumToUpdate).State = EntityState.Modified;
               db.SaveChanges();
               TempData["id"] = id;
               return RedirectToAction("Index");//,new
               // { 
               //    albumId = albumToUpdate.AlbumId });
               // }
            }
            catch (DataException)
            {
               ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator");
            }
         }

         PopulateAlbumCat(albumToUpdate);
         TempData["id"] = id;
         // return View(albumToUpdate);
         return View("Index");

      }


      [HttpGet]
      public ActionResult Upload(int id)
      {
       
            Album album = db.Albums
            .Include(i => i.Categories)
            .Where(i => i.AlbumId == id)
            .Single();

            return View(album);
   
       
      }

    
      

      [HttpPost]
      public ActionResult Upload(HttpPostedFileBase file, int id, string description)
      {
         var albumToUpdate = db.Albums
            .Single(t => t.AlbumId == id);      //album     

         if (file != null && file.ContentLength > 0)
         {
            string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf", ".JPG" };

            if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
            {
               ModelState.AddModelError("File", "only file of type: " + string.Join(", ", AllowedFileExtensions)+"are allowed");
            }
            var fileName = System.IO.Path.GetFileName(file.FileName);
            var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
            file.SaveAs(path);
          
               /***Begin db savin ***/

               Photo newphoto = new Photo(path);
               newphoto.FileName = System.IO.Path.GetFileName(file.FileName);//fileName;
               newphoto.AlbumId = id;

               if (description == null)
                  newphoto.Description = " ";
               else
                  newphoto.Description = description;
               using (MemoryStream ms = new MemoryStream())
               {
                  file.InputStream.CopyTo(ms);
                  newphoto.Picture = ms.GetBuffer();

               }
               if (ModelState.IsValid)
               {
               db.Photos.Add(newphoto);
               db.SaveChanges();
               ViewBag.Description = newphoto.Description;
               return RedirectToAction("Index");
               /**End db Saving **/
            }

           
         }

         return View(albumToUpdate);
      }



      public ActionResult Delete(int id)
      {
         // Tip tip = db.Tips.Find(id);
         Album album = db.Albums.Find(id);
         return View(album); //View(tip);
      }


    
      [HttpPost]
      public ActionResult DeletePicture(int id)
      {
         //if (Request.IsAjaxRequest())
         //{
            Photo photo = db.Photos.Find(id);
            if (photo != null)
            {
               ViewData["message"] = string.Format("Deleted Id '{0}' from the database!", id);
               var pId = photo.AlbumId.ToString();
               db.Photos.Remove(photo);
               db.SaveChanges();
            }
            
        
          return PartialView("Details/"+ photo.AlbumId);
        // }
        
      }


      public ActionResult IndexData(int id)
      {
         ViewBag.Id = id;
         ViewBag.Title = "The ID is:  " + id;
         return PartialView();

      }
      }

   }

     
         








