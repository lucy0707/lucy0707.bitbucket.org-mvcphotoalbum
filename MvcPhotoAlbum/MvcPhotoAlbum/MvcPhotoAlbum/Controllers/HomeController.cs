﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPhotoAlbum;
using MvcPhotoAlbum.Models;


namespace MvcPhotoAlbum.Controllers
{
   public class HomeController : Controller
   {
      private PhotoAlbumContext db = new PhotoAlbumContext();

      public ViewResult Index()
      {
         ViewBag.Message = " ";
         int hour = DateTime.Now.Hour;
         ViewBag.Greeting = hour < 12 ? "Good morning" : "Good afternoon";// terniary operator 
         return View();

        
      }

      //adding one more comment to test
      public ActionResult About()
      {
         ViewBag.Message = "Your app description page.";

         return View();
      }


      public ActionResult Contact()
      {
         ViewBag.Message = "Your contact page.";

         return View();
      }

   }
}
