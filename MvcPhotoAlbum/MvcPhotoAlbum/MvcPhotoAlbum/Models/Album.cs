﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace MvcPhotoAlbum.Models
{
   public class Album
   {
       // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      [Required]
      public  int AlbumId { get; set; }

      public virtual int UserId { get; set; }//FK
     
      [Required]
      [StringLength(30)]
      public string Title { get; set; }
    
      [DisplayFormat(DataFormatString = "{0:MM/dd/yy}", ApplyFormatInEditMode=true)]
      [Display(Name = "Date Taken")]
      public DateTime? DateTaken { get; set; }
     // public UserProfile UserName { get; set; }
      //navigation property
      public virtual ICollection< Category> Categories { get; set; }
      public virtual ICollection<Photo> Photos { get; set; }

     
     
      //adding photos to the album list
         
      //Constructor
      public Album(string albumName)
      {
         //we will create list of objects
        Photos = new List<Photo>();
       
        // Categories = new List<Category>();
         Title = albumName;

      }

      public Album()
      {

      }

      //public Album()
      //{
      //   Categories = new ICollection<Category>();
      //}
   }
}