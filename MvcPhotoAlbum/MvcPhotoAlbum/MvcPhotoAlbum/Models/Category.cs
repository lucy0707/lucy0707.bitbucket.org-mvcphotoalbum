﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPhotoAlbum.Models
{
   public class Category
   {
      public string CategoryName { get; set; }
      //public int CategoryId{ get; set; }
      public int CategoryId { get; set; }
     

      //navigation property
      public virtual ICollection< Album>  Albums { get; set; }
      
    //Constructor
        public Category(string w)
       {
        CategoryName = w;
       }

        public Category()
        {
        }
   }
}