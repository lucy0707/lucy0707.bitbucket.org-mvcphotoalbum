﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcPhotoAlbum.Models
{
   public class FilterClass
   {
      [Required]
      public string CategoryFilter { get; set; }
      
      [Required]
      public string TitleFiletr { get; set; }

      [Required]
      public string DateTaken { get; set; }

   }
}