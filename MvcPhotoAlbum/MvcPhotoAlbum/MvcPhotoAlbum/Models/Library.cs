﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcPhotoAlbum.Models
{
   public class Library
   {
      PhotoAlbumContext libraryDB = new PhotoAlbumContext();
      
      string LibraryId { get; set; }
      public const string LibrarySessionKey = "LibraryId";


      public static Library GetLibrary(HttpContextBase context)
      {
         var myLibrary = new Library();
         myLibrary.LibraryId = myLibrary.GetLibraryId(context);
         return myLibrary;
      }
      //using httpContextBase to allow access to cookies
      public string GetLibraryId(HttpContextBase context)
      {
         if (context.Session[LibrarySessionKey] == null)
         {
            if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
            {
               context.Session[LibrarySessionKey] = context.User.Identity.Name;
            }
            else
            {
               // Generate a new random GUID using System.Guid class
               Guid tempLibraryId = Guid.NewGuid();

               // Send tempCartId back to client as a cookie
               context.Session[LibrarySessionKey] = tempLibraryId.ToString();
            }
         }
         return context.Session[LibrarySessionKey].ToString();
      }

      //public void MigrateMyLibrary(string userName)
      //{
      //   var library = libraryDB.Library.Where(c => c.LibraryId == PersonalLibraryId);

      //   foreach (Library item in library)
      //   {
      //      item.LibraryId = userName;
      //   }
      //   libraryDB.SaveChanges();
      //}
   }
}