﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace MvcPhotoAlbum.Models
{
   public class Photo
   {
      [Required ]
      [FileExtensions(Extensions="jpg,png")]
      [UIHint("Image")]
      public string FileName { get; set; }
     
      public int PhotoId { get; set; }
      public virtual int AlbumId { get; set; }//FK
      public byte[] Picture { get; set; }//testing 
      public virtual string Description { get; set; }
  

      //constructor
      public Photo(string file)
      {
         FileName = file;
      }

      public Photo()
      {


      }

   }
}
