﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using MvcPhotoAlbum.Models;
using System.Data.Entity.Migrations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MvcPhotoAlbum.Models

{
   public class PhotoAlbumContext : DbContext
   {
      public PhotoAlbumContext() : base("MvcPhotoAlbum") { }

      public DbSet<Album> Albums { get; set; }
      public DbSet<Category> Categories { get; set; }
      public DbSet<Photo> Photos { get; set; }

      [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      public DbSet<UserProfile>UserProfile{ get; set; }

      protected override void OnModelCreating(DbModelBuilder modelBuilder)
      {
       
         modelBuilder.Entity<Album>()
            .HasMany(t => t.Categories).WithMany(t => t.Albums)
              .Map(t => t.MapLeftKey("AlbumId")
              .MapRightKey("CategoryId")
            .ToTable("AlbumCategory"));
      }
      

   }

   //public class AlbumInitializer : DropCreateDatabaseIfModelChanges<PhotoAlbumContext>
   //{
   //   protected override void Seed(MvcPhotoAlbum.Models.PhotoAlbumContext context)
   //   {


   //      var Albums = new List<Album>
   //        {
   //          // new Album{Title="Vacaciones en Mexico", DateTaken=DateTime.Today},
   //           new Album{Title="Cancun 2011", DateTaken=Convert.ToDateTime("07/07/2011")},
   //           new Album{Title="Miscellaneous"},
   //           new Album{Title="Belize", DateTaken=Convert.ToDateTime("07/01/2012")},
   //           new Album{Title="Guanajuato 2010",DateTaken=Convert.ToDateTime("07/01/2010")}

              
   //          // new Album ("vacaciones") { Title = "Vacaciones en Mexico " , AlbumId = 30, DateTaken= DateTime.Today}
   //     };
   //      Albums.ForEach(s => context.Albums.Add(s));
   //      context.SaveChanges();

   //      // User user = new User("Nico Castaldo");
   //     // context.Users.AddOrUpdate(p => p.UserName, new User { UserName = "Domenico Castaldo", Email = "nico@yahoo.com" },
   //       //                                      new User { UserName = "Emma Littlejohn", Email = "Emma@yahoo.com" }
   //       //                                      );
   //      context.SaveChanges();

   //      var category = new List<Category>
   //     {
   //        new Category{ CategoryName = "Sports" },
   //        new Category{ CategoryName = "Family" },
   //        new Category{ CategoryName = "Travel" },
   //        new Category{ CategoryName = "Nature" },
   //        new Category{ CategoryName = "Yoga" },
   //        new Category{ CategoryName = "Holidiays"}
   //     };
   //      category.ForEach(s => context.Categories.Add(s));
   //      context.SaveChanges();

   //   }
   //}

}