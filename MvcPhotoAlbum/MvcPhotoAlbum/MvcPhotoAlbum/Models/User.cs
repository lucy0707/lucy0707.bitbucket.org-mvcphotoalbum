﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using System.Web.Mvc;
using System.Globalization;

namespace MvcPhotoAlbum.Models
{
   public class User
   {
      public int UserId { get; set; }
       [Display(Name = "User name")]
      public string UserName { get; set; }
     // public string Email { get; set; }
      public virtual List<Album> Albums { get; set; }
      
     
     

     // [Required]
      [DataType(DataType.EmailAddress)]
      [Display(Name = "Email address")]
      public string Email { get; set; }

    //  [Required]
      [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = "Password")]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = "Confirm password")]
     // [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
      public string ConfirmPassword { get; set; }

      public User(string n,string email)
      {
         UserName = n;
         Email = email;
         //Albums = new List<Album>();

      }

      public User( UserProfile user)
      {
         UserId = user.UserId;
      }

      public User()
      {

      }
      

   }
}