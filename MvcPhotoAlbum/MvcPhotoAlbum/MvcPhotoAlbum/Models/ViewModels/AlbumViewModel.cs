﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcPhotoAlbum.Models;

namespace MvcPhotoAlbum.Models
{
   public class AlbumViewModel
   {
      public int AlbumId { get; set; }
      public string Title { get; set; }
      public virtual ICollection<AssignedCat> Categories { get; set; }

       
      public int? UserId { get; set; }//FK
     
      public int? CategoryId { get; set; } //FK
      //public  Photo PhotoFile  { get; set; }
     // public Category PicCategory { get; set; }
      public DateTime DateTaken { get; set; }
      public User UserName { get; set; }
      //navigation property
     // public virtual ICollection< Category> Categories { get; set; }
    
     

      public virtual ICollection<Photo> Photos { get; set; }

     
     //rivate List<Photo> photoList;
   // public virtual ICollection<Category> categories;

      //adding photos to the album list
      //public void addPhotos(string text, User theUser)
      //{
      //   Photo pic = new Photo(text);
      // Photos.Add(pic);
      //  pic.UserName = theUser;
      //}
    
      //Constructor
      public AlbumViewModel(string albumName)
      {
         //we will create list of objects
        Photos = new List<Photo>();
        // Categories = new List<Category>();
         Title = albumName;

      }

      public AlbumViewModel()
      {

      }
   }
}