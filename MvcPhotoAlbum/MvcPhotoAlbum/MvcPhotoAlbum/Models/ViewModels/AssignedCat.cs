﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcPhotoAlbum.Models;



   public class AssignedCat
   {
      public int CategoryId { get; set; }
      public string CategoryName { get; set; }
      public bool Assigned { get; set; }
   }
