﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcPhotoAlbum.Models;



   public class CategoryViewModel
   {
      public int CategoryID { get; set; }
      public int CategoryName { get; set; }
      public virtual ICollection<AlbumViewModel> Album { get; set; }
   }
