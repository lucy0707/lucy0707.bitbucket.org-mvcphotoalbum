﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Microsoft;
using Microsoft.Web.Helpers;

namespace MvcPhotoAlbum.Models.ViewModels
{
   public class ProfileViewModel
   {
       [Required]
     [FileExtensions(Extensions = ".jpg,.png,.jpeg", ErrorMessage = "Please Choose a File")]
      [UIHint("ProfileImage")]
      public string ImageUrl { get; set; }
      public virtual int AlbumId { get; set; }
   }
}