﻿SET IDENTITY_INSERT [dbo].[Photos] ON
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (8, N'DSCF0954nf.jpg', NULL, 4, N'Description')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (9, N'DSCF0617nf.jpg', NULL, 4, N'Description')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (10, N'25245_1428350626425_1927104_n.jpg', NULL, 2, N'Description')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (11, N'26165_1418798827636_6677931_n.jpg', NULL, 2, N'Lucy and Whitney Cancun')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (12, N'31807_1514575261987_6638806_n.jpg', NULL, 5, N'Plaza de la Paz')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (13, N'36784_1520449008827_3147891_n.jpg', NULL, 5, N'Tequilera Corralejo')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (14, N'150532_1761368431662_5387247_n.jpg', NULL, 3, N'Thanksgiving with Hannan')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (15, N'398113_208245449304345_571895239_n.jpg', NULL, 3, N'Description')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (16, N'402309_3097261028142_1623333758_n.jpg', NULL, 3, N'Lord of the Dance pose (Natarajasana)')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (17, N'406211_4375945754461_880992605_n.jpg', NULL, 3, N'Description')
INSERT INTO [dbo].[Photos] ([PhotoId], [FileName], [UserId], [AlbumId], [Description]) VALUES (18, N'8821_1240551371561_2472518_n.jpg', NULL, 3, N'Bird of Paradise')
SET IDENTITY_INSERT [dbo].[Photos] OFF
